# Codebase release 2.0 for UFOManager

by Mark S. Neubauer, Avik Roy, Zijun Wang

SciPost Phys. Codebases 13-r2.0 (2023) - published 2023-06-09

[DOI:10.21468/SciPostPhysCodeb.13-r2.0](https://doi.org/10.21468/SciPostPhysCodeb.13-r2.0)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.13-r2.0) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Mark S. Neubauer, Avik Roy, Zijun Wang

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.13-r2.0](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.13-r2.0)
* Live (external) repository at [https://github.com/Neubauer-Group/UFOManager/tree/v2.0.0](https://github.com/Neubauer-Group/UFOManager/tree/v2.0.0)
